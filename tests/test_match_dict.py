#!/usr/bin/ python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import pytest
sys.path.append(os.path.realpath('../'))
import client as client  # noqa E402

test_dict1 = {"name": "Michael", "Rafael": "Nadal"}
test_dict2 = {"name": "Michael", "Rafael": ""}


def test_missing_keys():
    var = client.RdosClient.missing_keys(client.RdosClient, test_dict1, test_dict2)
    assert not var


def test_bad_missing():
    with pytest.raises(Exception):
        assert client.RdosClient.missing_keys(client.RdosClient, {}, test_dict1)
