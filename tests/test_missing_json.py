#!/usr/bin/env python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import json
import pytest
sys.path.append(os.path.realpath('../'))
import client  # noqa E402

data1 = '{"id": "", "name": "", "state": "", "launch_date": "", "mdr_number": "", "nameC": {"first": "", "last": ""}, "title": "", "id": ""}'
data2 = '{"id": "", "name": "", "state": "", "launch_date": "", "mdr_number": "", "id": ""}'

obj = (json.loads(data1))
obj2 = (json.loads(data2))


def test_missing_json():
    var = client.RdosClient.missing_keys(client.RdosClient, obj, obj)
    assert var == []


def test_diff_missing_json():
    result = client.RdosClient.missing_keys(client.RdosClient, obj, obj2)
    assert result is not None


def test_bad_match_json():
    with pytest.raises(Exception):
        assert client.RdosClient.missing_keys(client.RdosClient, {}, obj)
