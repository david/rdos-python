#!/usr/bin/env python3
# Authors : Julien DAVID & Ismail MOUMNI
import sys
import os
import json
import pytest
sys.path.append(os.path.realpath('../'))
import client  # noqa E402

data = '{"country": "usa", "people": ""}'
bad_data = '{"sasd": "", "nam": {"saasd": "llam"} }'
obj = json.loads(data)
obj2 = json.loads(bad_data)


def test_match_dict():
    var = client.RdosClient.missing_keys(client.RdosClient, obj, obj)
    assert var == []


def test_diff_match_dict():
    result = client.RdosClient.missing_keys(client.RdosClient, obj, obj2)
    assert result is not None


def test_bad_match_dict():
    with pytest.raises(Exception):
        assert client.RdosClient.missing_keys(client.RdosClient, {}, obj)
