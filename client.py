#!/usr/bin/env python3
# Authors : Julien DAVID & Ismail MOUMNI
import socket
import json

# List to save REQUEST Parameters send FROM SERVER for query Match
__RDOS_Req__ = {"parameters": "request"}
__RDOS_Gen__ = {}


class RdosClient():

    __RDOS_Host__ = ''
    __RDOS_Port__ = ''

    # Function client_init connects to server and sends JSON Parameters needed
    # Function input Host : Server IP - Port : Server Port
    # Function output returns JSON from server side contaning parameters
    def __init__(self, addr, port):
        self.__RDOS_Gen__ = {}
        self.__RDOS_Host__ = addr
        self.__RDOS_Port__ = port
        data = {"parameters": "request"}
        socket = self.server_conn(self.__RDOS_Host__, self.___RDOS_Port__)
        print("Connexion to socket : ")
        socket.send(bytes(json.dumps(data), "utf-8"))
        recv = socket.recv(4096)
        self.__RDOS_Gen__ = (json.loads(recv.decode('utf-8')))

    # Function insert_query takes a query and send it to server for inserting a new job
    # Function insert_query parameters : Dictionary
    def insert_query(self, data: dict):
        if (data is not None):
            socket = self.server_conn(self.__RDOS_Host__, self.__RDOS_Port__)
            req = json.dumps((data))
            print("data send to server :", req)
            socket.send(bytes((req), "utf-8"))
            recv = socket.recv(4096)
            query = (recv.decode('utf-8'))
            print("data received:", query)
        else:
            raise Exception("Dictionnaire Vide !!")

    def default_values_generator(self, generator):
        query = {}
        if generator is not None:
            query['default'] = generator
            socket = self.server_conn(self.__RDOS_Host__, self.__RDOS_Port__)
            req = json.dumps(query)
            print("data send to server :", req)
            socket.send(bytes((req), "utf-8"))
            recv = socket.recv(4096)
            resp = (recv.decode('utf-8'))
            print("data received:", resp)
            return resp

    # Function get_objects connects client to server by socket
    # Function get_objects takes 2 parameters
    # Function get_objects returns the state of the server
    def get_objects(self):
        try:
            s = self.server_conn(self.__RDOS_Host__, self.__RDOS_Port__)
            message = json.dump((__RDOS_Req__))
            recv = self.send_st(s, message)
            print("Message ENVOYE : ", recv)
            return recv
        except socket.error as msg:
            print("Erreur de Connexion vers le client:" % msg)

    # Function missing_keys returns non existing fields in a dict
    # Function input biblio and dict
    # Function returns a list containing missing values else raises an Exception
    def missing_keys(self, biblio: dict, gen: dict):
        missing = []
        if(biblio is not None and gen is not None):
            for key in biblio:
                if key not in gen:
                    missing.append(key)
            return missing
        else:
            raise Exception("Dictionnaire Vide!!")

    # Function match_generator_dict Matches Dictionnary with a dictionnary
    # Function input 2 dictionaries to match
    # Function output true if dictionaries match else returns list of missing keys
    def match_query_dict(self, biblio: dict, data: dict):
        if biblio is not None and data is not None:
            return biblio.keys() == data.keys()
        else:
            return(self.missing_keys(biblio, data))

    # Function send_st sends to socket data bytes
    # Function input socket and data
    def send_st(self, s: socket, data: str):
        s.send(bytes(data, "utf-8"))

    # Function send_json_server sends dictionnary contaning Data in JSON Format
    # Function takes a HOST Port TO connect with server and a Dictionnary
    # Function prints Data send
    def send_js_server(self, Host, Port, biblio: dict):
        if not biblio:
            print("Dictionnary is Empty ")
        else:
            data = self.dict_to_JSON(biblio)
            self.run_client(Host, Port, data)
            print("Data send to server :", data)

    # Function server_conn creates a socket and connects it to server
    # Function server_conn take No arguments
    # Function returns a socket after making connexion
    def server_conn(self, host, port):
        self.serve_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.serve_sock.connect((host, port))
            return self.serve_sock
        except self.socket.error as exc:
            print("%s" % exc)

    # Function dict_to_JSON transforms DICTIONNARY TO Json String
    # Function input Dictionnary
    # Function OUTPUT Json string Format
    def dict_to_JSON(self, biblio: dict):
        if(biblio is not None):
            if (self.missing_keys(biblio) == []):
                return json.dumps(biblio)
            else:
                for a in __RDOS_Gen__:
                    print("Parameter: {}, Value : {}".format(a, __RDOS_Gen__[a]))
        else:
            raise Exception("Dictionnaire Vide!!")

    # Function json_to_dict changes a json file into a Dict
    # Function takes a json dict as input and returns a dictionary
    def json_to_dict(self, js):
        if js is not None:
            data = json.loads(js)
            return data
        else:
            raise Exception("Dictionnaire Vide!!")

    # Function get_generators returns a list of all available generetaros$
    def get_generators(self):
        if self.__RDOS_Gen__ is not None:
            return list(self.__RDOS_Gen__.keys())
        else:
            return Exception("DICTIONNAIRE Vide !!")

    # Function help prints the generators parameters
    # Function returns generator parameters
    def help(self, generator):
        print('Generator parameters :')
        default = self.default_values_generator(generator)
        print("Parameter:", list(json.loads(default))[0])
        print("Default Values : ", list(json.loads(default))[1])
